CREATE TABLE `Users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`users_nickname` varchar(255) NOT NULL,
	`users_first_name` varchar(255),
	`users_last_name` varchar(255),
	`users_email` varchar(255) NOT NULL,
	`users_password` varchar(10) NOT NULL,
	`users_avatar` varchar NOT NULL,
	`users_bio` varchar(255) NOT NULL,
	`created_on` DATE NOT NULL,
	`users_status` varchar(11) NOT NULL,
	`admin_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Content_points` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`points_daily` INT,
	`points_total` INT,
	`content_call_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Admin` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`admin_first_name` varchar(255) NOT NULL,
	`admin_last_name` varchar(255) NOT NULL,
	`admin_email` varchar(255) NOT NULL,
	`admin_password` varchar(10) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Content_calls` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`cc_name` varchar(255) NOT NULL,
	`cc_status` varchar(11) NOT NULL,
	`cc_categories` varchar NOT NULL,
	`cc_start_on` DATETIME NOT NULL,
	`cc_completed_on` DATETIME NOT NULL,
	`users_fk` INT NOT NULL,
	`admin_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Content_upload` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`content_image` varchar NOT NULL,
	`content_video` varchar NOT NULL,
	`content_text` varchar NOT NULL,
	`content_call_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Post` (
	`id` INT NOT NULL,
	`post_title` varchar(11) NOT NULL,
	`post_name` varchar(255) NOT NULL,
	`post_content` TEXT NOT NULL,
	`post_slug` varchar NOT NULL,
	`post_date` DATETIME NOT NULL,
	`post_status` varchar(11) NOT NULL,
	`admin_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Missions` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`status` varchar(11) NOT NULL,
	`start_on` DATETIME NOT NULL,
	`completed_on` DATETIME NOT NULL,
	`users_fk` INT NOT NULL,
	`admin_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Missions_points` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`points_daily` INT,
	`points_total` INT,
	`missions_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Coupon_code` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`coupon_name` varchar(255) NOT NULL,
	`coupon_status` varchar(11) NOT NULL,
	`coupon_started_on` DATETIME NOT NULL,
	`coupon_completed_on` DATETIME NOT NULL,
	`admin_fk` INT NOT NULL,
	`content_call_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Daily_affirmations` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`da_name` varchar(255) NOT NULL,
	`da_status` varchar(11) NOT NULL,
	`da_date` DATETIME NOT NULL,
	`admin_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Post_categories` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`p_categories_title` varchar(255) NOT NULL,
	`p_categories_name` varchar(255) NOT NULL,
	`p_categories_slug` varchar NOT NULL,
	`post_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Content_calls_categories` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`cc_categories_title` varchar(255) NOT NULL,
	`cc_categories_name` varchar(255) NOT NULL,
	`cc_categories_slug` varchar NOT NULL,
	`content_call_fk` INT NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `Points` ADD CONSTRAINT `Points_fk0` FOREIGN KEY (`content_call_fk`) REFERENCES `Content_upload`(`id`);

ALTER TABLE `Content_calls` ADD CONSTRAINT `Content_calls_fk0` FOREIGN KEY (`users_fk`) REFERENCES `Users`(`id`);

ALTER TABLE `Content_calls` ADD CONSTRAINT `Content_calls_fk1` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `Content_upload` ADD CONSTRAINT `Content_upload_fk0` FOREIGN KEY (`content_call_fk`) REFERENCES `Content_calls`(`id`);

ALTER TABLE `Post` ADD CONSTRAINT `Post_fk0` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `Missions` ADD CONSTRAINT `Missions_fk0` FOREIGN KEY (`users_fk`) REFERENCES `Users`(`id`);

ALTER TABLE `Missions` ADD CONSTRAINT `Missions_fk1` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `Missions_points` ADD CONSTRAINT `Missions_points_fk0` FOREIGN KEY (`missions_fk`) REFERENCES `Missions`(`id`);

ALTER TABLE `Coupon_code` ADD CONSTRAINT `Coupon_code_fk0` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `Coupon_code` ADD CONSTRAINT `Coupon_code_fk1` FOREIGN KEY (`content_call_fk`) REFERENCES `Content_calls`(`id`);

ALTER TABLE `Daily_affirmations` ADD CONSTRAINT `Daily_affirmations_fk0` FOREIGN KEY (`admin_fk`) REFERENCES `Admin`(`id`);

ALTER TABLE `post_categories` ADD CONSTRAINT `post_categories_fk0` FOREIGN KEY (`post_fk`) REFERENCES `Post`(`id`);

ALTER TABLE `Content_calls_categories` ADD CONSTRAINT `Content_calls_categories_fk0` FOREIGN KEY (`content_call_fk`) REFERENCES `Content_calls`(`id`);

